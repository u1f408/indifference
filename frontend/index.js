import { h, render, Component } from 'preact'
import 'whatwg-fetch'

import Main from './components/main.jsx'

const appElement = document.getElementById("application")
render(<Main />, appElement)
