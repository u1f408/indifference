import { h, Component } from 'preact'
import SidebarTimezone from './sidebar-timezone.jsx'

export default class Sidebar extends Component {
	render = (props, state) => {
		var timezones = props.timezones.map((tz) => {
			return (
				<SidebarTimezone zone={tz.zone} friendly={tz.friendly} />
			)
		})

		return (
			<div class="sidebar-timezones">
				{timezones}
			</div>
		)
	}
}

