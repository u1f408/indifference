import { h, Component } from 'preact'
import Action from './action.jsx'

export default class Group extends Component {
	render = (props, state) => {
		var actions = props.actions.map((action) => {
			return (
				<Action actionkey={action.key} text={action.text} type={action.type} actionstyle={action.style}/>
			)
		})

		return (
			<div class="group" data-name={props.name}>
				<h1>{props.name}</h1>
				<div class="group-actions">
					{actions}
				</div>
			</div>
		)
	}
}

