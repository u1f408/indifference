import { h, Component } from 'preact'
import moment from 'moment-timezone'

export default class SidebarTimezone extends Component {
	constructor() {
		super()

		this.state.time = moment()
	}

	componentDidMount = () => {
		this.timer = setInterval(() => {
			this.setState({ time: moment() })
		}, 200)
	}

	componentWillUnmount = () => {
		clearInterval(this.timer)
	}

	render = (props, state) => {
		let time = state.time.tz(props.zone).format('ddd HH:MM:ss')

		return (
			<div class="sidebar-timezone" data-timezone={props.tz}>
				<strong>{time}</strong>
				<span>{props.friendly}</span>
			</div>
		)
	}
}

