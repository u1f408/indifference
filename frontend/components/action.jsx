import { h, Component } from 'preact'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner, faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'

export default class Action extends Component {
	constructor() {
		super()

		this.state = {
			is_acting: false,
			action_success: null,
		}
	}

	doAction = (e) => {
		e.preventDefault()

		this.setState({is_acting: true})

		let data = {
			key: this.props.actionkey,
			value: e.target.value,
		}

		fetch("/action.json", {
			method: "POST",
			body: new Blob(
				[JSON.stringify(data)],
				{type: "application/json"}
			)
		}).then((resp) => {
			return resp.json()
		}).then((data) => {
			this.setState({action_success: true})
		}).catch((e) => {
			this.setState({action_success: false})
		}).finally(() => {
			setTimeout(() => {
				this.setState({is_acting: false, action_success: null})
			}, 2500)
		})

		return false
	}

	render = (props, state) => {
		let classes = `action action-${props.type} style-${props.actionstyle}`

		if (props.type === "button") {
			var progress = (<div class="action-progress hidden" />);
			if (state.is_acting) {
				if (state.action_success === null) {
					progress = (
						<div class="action-progress action-progress-running">
							<FontAwesomeIcon icon={faSpinner} fixedWidth spin size="2x" />
						</div>
					)
				} else if (state.action_success === true) {
					progress = (
						<div class="action-progress action-progress-success">
							<FontAwesomeIcon icon={faCheck} fixedWidth size="2x" />
						</div>
					)
				} else {
					progress = (
						<div class="action-progress action-progress-failure">
							<FontAwesomeIcon icon={faTimes} fixedWidth size="2x" />
						</div>
					)
				}
			}

			return (
				<div class={classes}>
					<button onClick={this.doAction} data-key={props.actionkey}>
						{props.text}
					</button>

					{ progress }
				</div>
			)
		}

		return (
			<div class="hidden" style="display: none">
				unknown action type {props.type} for key {props.actionkey}
			</div>
		)
	}
}

