import { h, Component } from 'preact'
import Group from './group.jsx'
import Sidebar from './sidebar.jsx'

export default class Main extends Component {
	constructor() {
		super()

		this.state = {
			groups: null,
			timezones: null,
			loaded: false,
			error: null,
		}
	}

	componentDidMount = () => {
		this.getConfig()
	}

	getConfig = () => {
		fetch("/config.json", {
			method: "GET",
		}).then((resp) => {
			return resp.json()
		}).then((data) => {
			console.log(data)

			this.setState({
				groups: data["groups"],
				timezones: data["dashboard"]["timezones"],
				loaded: true,
				error: null,
			})

			setTimeout(() => this.forceUpdate(), 1)
		}).catch((e) => {
			this.setState({
				groups: null,
				timezones: null,
				loaded: true,
				error: e,
			})

			setTimeout(() => this.forceUpdate(), 1)
		})
	}

	render = (props, state) => {
		if (state.loaded) {
			if (state.error === null) {
				var groups = state.groups.map((group) => {
					return (
						<Group name={group.name} actions={group.actions} />
					)
				})

				return (
					<div class="container">
						<div class="sidebar-container">
							<Sidebar timezones={state.timezones} />
						</div>

						<div class="group-container">
							{groups}
						</div>
					</div>
				)
			} else {
				return (
					<div class="error">
						{state.error}
					</div>
				)
			}
		}

		return (
			<div class="loading">
				Loading...
			</div>
		)
	}
}
