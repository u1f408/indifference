const path = require('path')

module.exports = {
	entry: "./frontend/index.js",
	output: {
		path: path.join(__dirname, "public"),
		filename: "indifference.js"
	},
	resolve: {
		alias: {
			"react": "preact-compat",
			"react-dom": "preact-compat"
		}
	},
	module: {
		loaders: [
			{
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['react', 'env'],
					plugins: [
						[
							"transform-react-jsx",
							{
								pragma: "h"
							}
						],
						[
							"transform-class-properties"
						]
					]
				}
			}
		]
	}
}
