# indifference

Buttons that do things that you tell them to.

## Running

Set up:

```
$ bundle install --path vendor/bundle/
$ npm install
$ grunt
```

Copy `config.yml.example` to `config.yml` and edit, and then:

```
$ bundle exec rackup
```

## License

MIT, see [LICENSE](./LICENSE)

