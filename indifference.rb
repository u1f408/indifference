require 'json'
require 'yaml'
require 'sinatra/base'

class Indifference < Sinatra::Base
	def self.validate_actions(groups)
		seen_groups = []
		seen_keys = []

		groups.each do |group, actions|
			raise "group #{group.inspect} already seen" if seen_groups.include?(group)
			seen_groups << group

			actions.each do |action|
				raise "key #{action[:key].inspect} already seen" if seen_keys.include?(action[:key])
				seen_keys << action[:key]
			end
		end
	end

	configure do
		@@config_path = ENV['INDIFFERENCE_CONFIG'] || File.expand_path('../config.yml', __FILE__)
		@@config = YAML::load_file(@@config_path)

		validate_actions @@config[:actions]
	end

	get '/' do
		send_file File.join(settings.public_folder, 'index.html')
	end

	get '/config.json' do
		groups = @@config[:actions].map do |group, actions_raw|
			actions = actions_raw.map do |action|
				{
					:key => action[:key],
					:text => action[:text],
					:type => action[:type],
					:style => action[:style],
				}
			end

			{
				:name => group,
				:actions => actions
			}
		end

		out = {
			:dashboard => {
				:timezones => @@config[:sidebar][:timezones],
			},
			:groups => groups,
		}

		content_type :json
		out.to_json
	end

	post '/action.json' do
		content_type :json

		begin
			data = JSON.parse(request.body.read.to_s)
		rescue JSON::ParserError
			return [500, {:error => "JSON parser error"}.to_json]
		rescue
			return [500, {:error => "Unknown error"}.to_json]
		end

		@@config[:actions].each do |group, actions|
			actions.each do |action|
				if action[:key] == data["key"]
					args = action[:action].map do |x|
						next data["value"] if x == "{}"
						x
					end

					pid = spawn(*args)
					Process.detach pid

					return {:success => true}.to_json
				end
			end
		end

		[404, {:error => "No key found"}.to_json]
	end
end
