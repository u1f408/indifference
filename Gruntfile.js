const webpackConfig = require('./webpack.config');

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			dist: {
				files: {
					'public/indifference.min.js': 'public/indifference.js'
				},
				options: {
					sourceMap: true
				}
			}
		},
		stylus: {
			dist: {
				options: {
					compress: true
				},
				files: {
					'public/styles.css': 'frontend/css/styles.styl'
				}
			}
		},
		webpack: {
			dist: webpackConfig
		}
	})

	grunt.loadNpmTasks('grunt-contrib-stylus')
	grunt.loadNpmTasks('grunt-webpack')
	grunt.loadNpmTasks('grunt-contrib-uglify')

	grunt.registerTask('default', [
		'stylus',
		'webpack',
		'uglify'
	])
}
